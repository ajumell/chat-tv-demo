import os, sys


def make_conf(domain, proj_dir, logs_dir, static_dir, wsgi_path, name):
    config_template = "<VirtualHost *:80>"

    if domain is not None:
        config_template += "ServerName %(domain)s"

    config_template += """
    CustomLog %(logs_dir)s/%(name)s-access_log common
    ErrorLog %(logs_dir)s/%(name)s-error_log

    Alias /static/ %(static_path)s
    <Directory %(static_path)s>
        Order deny,allow
        Allow from all
    </Directory>

    WSGIScriptAlias / %(wsgi_path)s
    <Directory %(project_dir)s>
        Order deny,allow
        Allow from all
    </Directory>
</VirtualHost>
    """

    d = {
        'name': name,
        'project_dir': proj_dir,
        'logs_dir': logs_dir,
        'static_path': static_dir,
        'wsgi_path': wsgi_path
    }

    if domain is not None:
        d['domain'] = domain

    config_str = config_template % d

    return config_str


def make_directories(static_dir, logs_dir):
    if not os.path.exists(static_dir):
        os.mkdir(static_dir)
    elif not os.path.isdir(static_dir):
        print "The static directory path is not a directory"
    else:
        print "The static directory already exists."

    if not os.path.exists(logs_dir):
        os.mkdir(logs_dir)
    elif not os.path.isdir(logs_dir):
        print "The logs directory path is not a directory"
    else:
        print "The logs directory already exists."


def collect_static(proj_dir):
    from django.core.management import execute_from_command_line

    if proj_dir not in sys.path:
        sys.path.append(proj_dir)

    key = 'DJANGO_SETTINGS_MODULE'
    env = os.environ
    prev_settings = os.environ.get(key, None)
    env.setdefault(key, "myproject.settings")
    execute_from_command_line(['manage.py', 'collectstatic', '--noinput'])

    if key in env:
        if prev_settings is not None:
            env[key] = prev_settings
        else:
            del env[key]


def read_domain():
    print "Please enter your domain name."
    print "There is no need for http:// or https://"
    print "eg: tv.xeoscript.com"

    domain = raw_input("Enter the domain name :")

    if not domain:
        domain = "tv.xeoscript.com"

    return domain


def write_file(name, contentes):
    fp = file(name, 'w')
    fp.write(contentes)
    fp.close()


def main():
    abspath = os.path.abspath
    join = os.path.join
    dirname = os.path.dirname

    single = True

    cur_dir = abspath(dirname(__file__))
    proj_dir = join(cur_dir, 'myproject')
    static_dir = join(cur_dir, 'static')
    logs_dir = join(cur_dir, 'logs')
    wsgi_path = join(proj_dir, 'myproject', 'wsgi.py')

    name = 'chat-tv'
    apache_conf_path = join(sys.argv[1], name + '.conof')

    domain = None if single else read_domain()
    conf = make_conf(domain, proj_dir, logs_dir, static_dir, wsgi_path, name)

    wsgi_path(apache_conf_path, conf)
    make_directories(static_dir, logs_dir)
    collect_static(proj_dir)


if __name__ == "__main__":
    main()