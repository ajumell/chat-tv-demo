sudo yum -y update

sudo yum -y install httpd
sudo yum -y install python27
sudo yum -y install python-setuptools
sudo yum -y install mod_wsgi
sudo yum -y install sqlite
sudo yum -y install python-sqlite

python2.7 generate-code.py /etc/httpd/conf.d/

/etc/init.d/httpd restart

