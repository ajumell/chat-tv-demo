#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    CUR_DIR = os.getcwd()
    sys.path.append(CUR_DIR+ '/../libs/')
    sys.path.append(CUR_DIR+ '/../lib/python2.7/')

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
