"""
This file contains the coding of the data tables in the project.
"""

from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from models import TextMessage, CensoringPhone, CensoringWord, RandomMessage
from django_helpers.apps.data_tables import DataTable, DataTableColumn, register, DataTableTemplateColumn

link_template = 'data-table/link.html'
delete_template = 'data-table/delete-btn.html'
message_status_template = 'data-table/message-status.html'


class RelatedFieldDataTableColumn(DataTableColumn):
    def get_html(self, request, instance):
        if self.field.find('.') > -1:
            parts = self.field.split('.')
        elif self.field.find('__') > -1:
            parts = self.field.split('__')
        else:
            return getattr(instance, self.field)

        for field in parts:
            instance = getattr(instance, field)

        return instance


class DateFieldDataTableColumn(DataTableColumn):
    def get_html(self, request, instance):
        return str(getattr(instance, self.field))


class LinkColumn(DataTableTemplateColumn):
    def __init__(self, field, text, color, link):
        DataTableTemplateColumn.__init__(self, field, '', False, False, link_template, title_width="20px")
        self.text = text
        self.color = color
        self.link = link

    def get_html(self, request, instance):
        value = getattr(instance, self.field)
        template = self.template

        try:
            link = reverse(self.link, args=(
                value,
            ))
        except:
            link = self.link

        context = RequestContext(request, {
            self.value_name: value,
            'link': link,
            'text': self.text,
            'color': self.color
        })

        return template.render(context)


class DeleteColumn(DataTableTemplateColumn):
    def __init__(self, field, link):
        DataTableTemplateColumn.__init__(self, field, _('Delete'), False, False, delete_template, title_width='20px')
        self.link = link
        self.width = 20

    def get_html(self, request, instance):
        value = getattr(instance, self.field)
        template = self.template

        try:
            link = reverse(self.link, args=(
                value,
            ))
        except:
            link = self.link

        context = RequestContext(request, {
            self.value_name: value,
            'link': link
        })

        return template.render(context)


class DataTableBase(DataTable):
    bootstrap_theme = False
    width = '100%'
    class_name = 'display'
    info_empty = _('Oops. No data found.')
    info_loading = _('Please wait while the data is loading...')
    scroller = False
    dom = ''
    template = 'includes/table.html'


@register
class PendingMessageDataTable(DataTableBase):
    table_id = 'pending-message'
    columns = [
        RelatedFieldDataTableColumn('prefix__text', _('Prefix'), True, True),
        DataTableColumn('message', _('Message'), True, True),
        DataTableColumn('number', _('Number'), True, True),
        LinkColumn('id', _('Approve'), 'green', 'approve-message'),
        LinkColumn('id', _('Block'), 'red', 'block-message'),
    ]

    def get_query(self, request, kwargs=None):
        return TextMessage.objects.pending()


@register
class VerifiedMessageDataTable(DataTableBase):
    table_id = 'verified-message'
    columns = [
        RelatedFieldDataTableColumn('prefix__text', _('Prefix'), True, True),
        DataTableColumn('message', _('Message'), True, True),
        DataTableColumn('number', _('Number'), True, True),
        DateFieldDataTableColumn('date_time', _('Date'), True, True),
        DataTableTemplateColumn('status', _('Status'), False, False, message_status_template),
    ]

    def get_query(self, request, kwargs=None):
        return TextMessage.objects.approved_or_published()


@register
class CensoredWordsDataTable(DataTableBase):
    table_id = 'censored-words'
    columns = [
        DataTableColumn('word', _('Word'), True, True),
        DeleteColumn('word', 'delete-censoring-word')
    ]

    def get_query(self, request, kwargs=None):
        return CensoringWord.objects.all()


@register
class CensoredPhoneDataTable(DataTableBase):
    table_id = 'censored-phone'
    columns = [
        DataTableColumn('number', _('Number'), True, True),
        DeleteColumn('number', 'delete-censoring-phone')
    ]

    def get_query(self, request, kwargs=None):
        return CensoringPhone.objects.all()


@register
class RandomMessageDataTable(DataTableBase):
    table_id = 'random-message'
    columns = [
        DataTableColumn('text', _('Message'), True, True),
        DeleteColumn('id', 'delete-random-message')
    ]

    def get_query(self, request, kwargs=None):
        return RandomMessage.objects.all()

