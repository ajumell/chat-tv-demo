from django.utils.translation import ugettext_lazy as _
from text_validation import validate
from django import forms
from models import *


class AddMessageForm(forms.ModelForm):
    def clean_number(self):
        cd = self.cleaned_data.get
        number = cd('number')
        if not number.isdigit():
            raise forms.ValidationError(_('A phone number must contain only digits.'))
        if CensoringPhone.objects.filter(number=number).count() > 0:
            raise forms.ValidationError(_('This phone number is censored.'))
        return number

    def clean_message(self):
        cd = self.cleaned_data.get
        message = cd('message')
        if not validate(message):
            raise forms.ValidationError(_('This message contains un allowed.'))
        return message

    class Meta:
        widgets = {
            'message': forms.Textarea()
        }
        model = TextMessage
        exclude = (
            'status',
            'date_time'
        )


class AddCensoredWordForm(forms.ModelForm):
    mode = forms.CharField(widget=forms.HiddenInput({
        'value': 'add'
    }))

    def clean_word(self):
        cd = self.cleaned_data.get
        word = cd('word')
        if not word.isalpha():
            raise forms.ValidationError(_('A word must contain only alphabets.'))
        return word

    class Meta:
        model = CensoringWord


class UploadCensoredWordForm(forms.Form):
    mode = forms.CharField(widget=forms.HiddenInput({
        'value': 'upload'
    }))
    text_file = forms.FileField(label=_('Text File (*.txt)'), widget=forms.FileInput({
        'class': 'file full'
    }))

    def save(self):
        uploadedfile = self.cleaned_data.get('text_file')
        count = 0
        for line in uploadedfile:
            line = line.strip()
            if line.isalpha():
                try:
                    obj = CensoringWord()
                    obj.word = line
                    obj.save()
                    count += 1
                except Exception:
                    pass
        return count


class AddCensoredPhoneForm(forms.ModelForm):
    mode = forms.CharField(widget=forms.HiddenInput({
        'value': 'add'
    }))

    def clean_number(self):
        cd = self.cleaned_data.get
        number = cd('number')
        if not number.isdigit():
            raise forms.ValidationError(_('A phone number must contain only digits.'))
        return number

    class Meta:
        model = CensoringPhone


class UploadCensoredPhoneForm(forms.Form):
    mode = forms.CharField(widget=forms.HiddenInput({
        'value': 'upload'
    }))
    text_file = forms.FileField(label=_('Text File (*.txt)'), widget=forms.FileInput({
        'class': 'file full'
    }))

    def save(self):
        uploadedfile = self.cleaned_data.get('text_file')
        count = 0
        for line in uploadedfile:
            line = line.strip()
            if line.isdigit():
                try:
                    obj = CensoringPhone()
                    obj.number = line
                    obj.save()
                    count += 1
                except Exception:
                    pass
        return count


class RandomMessageForm(forms.ModelForm):
    class Meta:
        model = RandomMessage


class PrefForm(forms.ModelForm):
    class Meta:
        model = Prefix
        exclude = (
            'text',
            'way_skirt',
        )