import os

from django.core.management import execute_from_command_line
from django.db import connection

from chat_tv.models import Prefix


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.settings")


def dummy_data():
    prefix = Prefix()
    prefix.text = 'IBER'
    prefix.message_mode = 'V'
    prefix.view_mode_display_time = 10
    prefix.chat_mode_display_time = 10
    prefix.moderator_mode = "M"

    prefix.background_color = "#787878"

    # Colors
    prefix.message_color = "#8BD1DF"
    prefix.message_text_color = "#FFFFFF"
    prefix.message_stripper_color = "#8BD1DF"
    prefix.box_background_color = "#015855"
    prefix.background_image = "hearts.jpg"

    # Marquee Color
    prefix.marquee_text_color = "#006CC7"
    prefix.marquee_highlighted_text_color = "#015855"
    prefix.marquee_background_color = "#8BD1DF"

    # Price text color
    prefix.price_text_color = "#ffffff"

    # Download Instructions
    prefix.color_download_instructions = "#006CC7"
    prefix.color_discharge_words = "#015855"
    prefix.color_number_of_downloads = "#ffffff"
    prefix.background_color_box_download = "#ffffcc"

    # Margins
    prefix.top_margin = 10
    prefix.left_margin = 10
    prefix.right_margin = 10
    prefix.bottom_margin = 10

    # Way Skirt
    prefix.way_skirt = 0

    # Fly
    prefix.show_fly = False

    # Responses
    prefix.ok_response = "Thanks for the message."
    prefix.number_censored_response = "Your phone number is censored."
    prefix.text_censored_response = "Your message contains censored words."

    prefix.save()


def recreate():
    try:
        cursor = connection.cursor()
        cursor.execute("DROP TABLE chat_tv_prefix")
        cursor.fetchone()
    except Exception:
        pass
    execute_from_command_line(['manage.py', 'syncdb'])
    dummy_data()


if __name__ == "__main__":
    print recreate()