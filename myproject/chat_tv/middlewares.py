class LocaleMiddleware(object):
    def process_response(self, request, response):
        if 'Content-Language' in response and response['Content-Language'] == 'none':
            del response['Content-Language']
            response['Content-Type'] = 'text/html'

            if 'Vary' in response: del response['Vary']

        return response
