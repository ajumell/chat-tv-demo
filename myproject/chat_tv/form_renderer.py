from django_helpers.apps.form_renderer import FormRenderer
from django.utils.translation import ugettext_lazy as _


class BaseFormRenderer(FormRenderer):
    template = 'includes/form.html'
    has_validation = True
    has_rules = False
    pre_processors = []


class LoginFormRenderer(BaseFormRenderer):
    form_submit = _('Login')
    form_id = 'login-form'


class MessageFormRenderer(BaseFormRenderer):
    form_submit = _('Add Message')
    form_id = 'add-message-form'


class AddCensoringWordFormRenderer(BaseFormRenderer):
    form_submit = _('Add Censoring Word')
    form_id = 'add-censoring-word-form'


class UploadCensoringWordFormRenderer(BaseFormRenderer):
    form_submit = _('Upload Censoring Word')
    form_id = 'upload-censoring-word-form'


class AddCensoringPhoneFormRenderer(BaseFormRenderer):
    form_submit = _('Add Censoring Number')
    form_id = 'add-censoring-phone-form'


class UploadCensoringPhoneFormRenderer(BaseFormRenderer):
    form_submit = _('Upload Censoring Number')
    form_id = 'upload-censoring-phone-form'


class RandomMessageFormRenderer(BaseFormRenderer):
    form_submit = _('Add Random Message')
    form_id = 'add-random-message-form'