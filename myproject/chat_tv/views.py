# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.messages import success
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from text_validation import new_msg
from forms import *
from form_renderer import *
from tables import *


def login_view(request):
    data = {
        'error': False,
    }

    if request.method == 'POST':
        gt = request.POST.get
        username = gt('user', "")
        password = gt('pass', "")

        if username == "" or password == "":
            data['error'] = True
            data['msg'] = _('Username or password is missing.')

        user = authenticate(username=username, password=password)
        if user is None:
            data['error'] = True
            data['msg'] = _('Invalid username or passowrd.')
        else:
            login(request, user)
            request.session.set_expiry(5 * 60 * 10)
            url = request.GET.get('next', '')
            if url == '':
                url = 'home'
            return redirect(url)

    return render(request, 'login.html', data)

def not_in_demo(request):
    return render(request, 'not-in-demo.html')

@login_required
def home(request):
    return render(request, 'home.html')


# Messages
@login_required
def add_message(request):
    if request.method == 'POST':
        form = AddMessageForm(request.POST)
        if form.is_valid():
            form.save()
            success(request, _('Message added successfully'))
            return redirect('manage-message')
    else:
        form = AddMessageForm()
    return render(request, 'form-page.html', {
        'form': MessageFormRenderer(form, request),
        'form_heading': _('Add Message'),
        'title': _('Add Message')
    })


@csrf_exempt
def add_message_cp(request):
    try:
        gt = request.REQUEST.get
        prefix = Prefix.objects.get(text=gt('ALIAS'))
        number = gt('SENDER')
        message = gt('TEXT')
        msg = TextMessage()

        if CensoringPhone.objects.filter(number=number).count() > 0:
            raise Exception(prefix.number_censored_response)

        msg.number = number
        msg.message = message
        msg.prefix = prefix

        if new_msg(message) != message:
            html = prefix.text_censored_response
        else:
            html = prefix.ok_response

        if prefix.is_automatic():
            msg.status = 'A'

        msg.save()
    except Exception, dt:
        html = unicode(dt)
    return HttpResponse(html)


@login_required
def manage_message(reqest):
    return render(reqest, 'manage-messages.html', {
        'pending_table': PendingMessageDataTable,
        'verified_table': VerifiedMessageDataTable
    })


@login_required
def block_message(request, message_id):
    obj = TextMessage.objects.get(id=message_id)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'OK':
            obj.status = 'B'
            obj.save()
            success(request, _('Message "%s" blocked successfully.') % obj.message)
            if request.is_ajax():
                return HttpResponse('true')
            return redirect('manage-message')

    msg = _('Do you want to block message "%(message)s" from "%(number)s" ?') % {
        'message': obj.message,
        'number': obj.number
    }

    url = reverse('manage-message')

    return render(request, 'confirm.html', {
        'box_title': _('Block Message'),
        'msg': msg,
        'back_link': url
    })


@login_required
def approve_message(request, message_id):
    obj = TextMessage.objects.get(id=message_id)
    if True:
        obj.status = 'A'
        obj.save()
        return redirect('manage-message')

    if request.method == 'POST':
        if request.POST.get('confirm') == 'OK':
            obj.status = 'A'
            obj.save()
            success(request, _('Message "%s" approve successfully.') % obj.message)
            if request.is_ajax():
                return HttpResponse('true')
            return redirect('manage-message')

    msg = _('Do you want to approve message "%(message)s" from "%(number)s" ?') % {
        'message': obj.message,
        'number': obj.number
    }
    url = reverse('manage-message')

    return render(request, 'confirm.html', {
        'box_title': _('Approve Message'),
        'msg': msg,
        'back_link': url
    })


# Censoring Words
@login_required
def censoring_words(request):
    return render(request, 'censoring-word.html', {
        'add_form': AddCensoringWordFormRenderer(add_form, request),
        'upload_form': UploadCensoringWordFormRenderer(upload_form, request),
        'table': CensoredWordsDataTable
    })


@login_required
def delete_censoring_word(request, word):
    obj = CensoringWord.objects.filter(word=word)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Delete':
            obj.delete()
            success(request, _('Word "%s" deleted successfully.') % word)
            if request.is_ajax():
                return HttpResponse('true')
            return redirect('censoring-words')

    msg = _('Do you want to delete the word "%s" ?') % word
    url = reverse('censoring-words')

    return render(request, 'delete-confirm.html', {
        'box_title': _('Delete word'),
        'msg': msg,
        'back_link': url
    })

# Censoring Numbers
@login_required
def censoring_phones(request):
    return render(request, 'censoring-phone.html', {
        'add_form': AddCensoringPhoneFormRenderer(add_form, request),
        'upload_form': UploadCensoringPhoneFormRenderer(upload_form, request),
        'table': CensoredPhoneDataTable
    })


@login_required
def delete_censoring_phone(request, number):
    obj = CensoringPhone.objects.filter(number=number)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Delete':
            obj.delete()
            success(request, _('Number "%s" deleted successfully.') % number)
            if request.is_ajax():
                return HttpResponse('true')
            return redirect('censoring-phones')

    msg = _('Do you want to delete the number "%s" ?') % number
    url = reverse('censoring-phones')

    return render(request, 'delete-confirm.html', {
        'box_title': _('Delete number'),
        'msg': msg,
        'back_link': url
    })


# Random Messages
@login_required
def random_messages(request):
    if request.method == 'POST':
        add_form = RandomMessageForm(request.POST)
        if add_form.is_valid():
            add_form.save()
            success(request, _('Random message added successfully'))
            add_form = RandomMessageForm()
    else:
        add_form = RandomMessageForm()

    return render(request, 'random-message.html', {
        'add_form': RandomMessageFormRenderer(add_form, request),
        'table': RandomMessageDataTable
    })


@login_required
def delete_random_message(request, message_id):
    obj = RandomMessage.objects.get(id=message_id)
    if request.method == 'POST':
        if request.POST.get('confirm') == 'Delete':
            obj.delete()
            success(request, _('Random message deleted successfully'))
            if request.is_ajax():
                return HttpResponse('true')
            return redirect('random-messages')

    msg = _('Do you want to delete the message "%s" ?') % obj.text
    url = reverse('random-messages')

    return render(request, 'delete-confirm.html', {
        'box_title': _('Delete message'),
        'msg': msg,
        'back_link': url
    })


@login_required
def sign_out(request):
    logout(request)
    return redirect('login')


def edit_preferences(request, keyword):
    return not_in_demo(request)


@login_required
def show_chat_window(request, keyword):
    prefix = Prefix.objects.get(text=keyword)
    if prefix.message_mode == 'V':
        swf = 'chat_tv_opinion_new'
    else:
        swf = 'chat_tv_multi_new'
        swf = 'chat_tv_multi_new(10-5-2013)-cs6'
    fs = "true" if prefix.on_air else "false"
    path = 'swf/%(swf)s.swf?KEYWORD=%(keyword)s&MSG_URL=%(msg_url)s&PREF_URL=%(pref_url)s&PRIVADOS=0&TELEFONO=0&CACHE=9625&FS=%(fullscreen)s' % {
        'keyword': keyword,
        'swf': swf,
        'msg_url': reverse('get-messages', args=(prefix, )),
        'pref_url': reverse('get-preferences', args=(prefix, )),
        'fullscreen': fs
    }
    return render(request, 'chat.html', {
        'path': mark_safe(path),
        'prefix': prefix,
        'fs':fs
    })


@login_required
def get_preference(request, keyword):
    prefix = Prefix.objects.get(text=keyword)
    d = {
        'BACKGROUND_SRC': prefix.background_image.strip(),

        'COLORCROMA': prefix.background_color[1:],

        'COLORFONDOMENSAJES': prefix.box_background_color[1:],

        'COLORTEXTOMARQUESINA': prefix.marquee_text_color[1:],
        'COLORDESTACADOMARQUESINA': prefix.marquee_highlighted_text_color[1:],
        'COLORFONDOMARQUESINA': prefix.marquee_background_color[1:],

        'COLORPRECIO': prefix.price_text_color[1:],

        'NUMERO_DESCARGA': u'27555',
        'COLORDESCARGATEXTO': prefix.marquee_text_color[1:],
        'COLORDESCARGAPALABRAS': prefix.color_discharge_words[1:],
        'COLORDESCARGANUMERO': prefix.color_number_of_downloads[1:],
        'COLORFONDODESCARGA': prefix.background_color_box_download[1:],


        'MODO_FALDON': u'3',
        'MOSCA': u'0',

        'PRECIO': u'<font size="8WPR,SA sms@wpr.es 902044008</font><br><font size="9PVP/SMS 1,45%26%238364; imp. inc.</font>',
        'PRECIO_FIJO': u'<b>FIJO: 1.16%E2%82%AC IVA INC.</b>',
        'PRECIO_MOVIL': u'<b>' + ugettext('MOBILE') + u': 1.51%E2%82%AC IVA INC.</b>',

        'TEXTOMARQUESINA1': u'%s: ' % ugettext('SEND TO CHAT ON TELEVISION'),
        'TEXTOMARQUESINA2': u'    WPR,SA sms@wpr.es 902044008',
        'TEXTOMARQUESINA3': u'%s: ' % ugettext('SEND TO PRIVATE'),

        'TEXTO_ENVIA_CHAT': u'',
        'TEXTO_ENVIA_OPINION': u'',
        'TIEMPO_MODO_CHAT': prefix.chat_mode_display_time,
        'TIEMPO_MODO_OPINION': prefix.view_mode_display_time,
        'WEB_PATH': settings.STATIC_URL + "img/img/",
    }

    xxx = """
    &WEB_PATH=http://chattv.nrs-group.com/&

    COLORCROMA=787878 Background Color
    COLORTEXTOMENSAJE=ffffff
    COLORFONDOMENSAJES=015855
    COLORTEXTOMARQUESINA=006cc7
    COLORDESTACADOMARQUESINA=015855
    COLORFONDOMARQUESINA=8BD1DF

    TEXTOMARQUESINA1=PARA CHATEAR EN TELEVISIÓN ENVÍA:
    TEXTOMARQUESINA2=    WPR,SA sms@wpr.es 902044008
    TEXTOMARQUESINA3=PARA PRIVADO ENVÍA:
    PRECIO=<font size="8WPR,SA sms@wpr.es 902044008</font><br><font size="9PVP/SMS 1,45%26%238364; imp. inc.</font>
    PRECIO_FIJO=<b>FIJO: 1.16%E2%82%AC IVA INC.</b>&PRECIO_MOVIL=<b>MÓVIL: 1.51%E2%82%AC IVA INC.</b>

    COLORPRECIO=ffffff
    NUMERO_DESCARGA=27555
    COLORDESCARGATEXTO=006cc7
    COLORDESCARGAPALABRAS=015855
    COLORDESCARGANUMERO=ffffff
    COLORFONDODESCARGA=FFFFCC

    BACKGROUND_SRC=hearts.jpg
    MODO_FALDON=3
    TEXTO_ENVIA_CHAT=
    TEXTO_ENVIA_OPINION=
    MOSCA=0
    TIEMPO_MODO_OPINION=10
    TIEMPO_MODO_CHAT=10
    """

    op = u'&'
    for x, y in d.items():
        op = op + x + '=' + str(y) + '&'
    return HttpResponse(op)


@login_required
def get_message(request, keyword):
    prefix = Prefix.objects.get(text=keyword)
    msg, new_id, show = TextMessage.objects.next_message(prefix, request.GET.get('LAST_ID', ''))
    msg = new_msg(msg)
    op = u'&CHATMESSAGES=<b><font color="%(color)s">%(text)s</font></b>&LAST_ID=%(last_id)d&SHOW=%(show)s' % {
        'color': prefix.message_color,
        'text': unicode(msg),
        'last_id': new_id,
        'show': show
    }
    res = HttpResponse(op)
    res['Content-Language'] = 'none'
    return res
