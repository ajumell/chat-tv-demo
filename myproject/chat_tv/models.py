# coding=utf-8
from random import randint
from django.db import models
from django.utils.translation import ugettext_lazy as _


STATUS_CHOICES = (
    ('P', _('Pending')),
    ('A', _('Approved')),
    ('B', _('Blocked')),
    ('S', _('Published')),
)

IMAGE_CHOICES = (
    ('transparent', "-- Ninguna --"),
    ("erotic.jpg", " Erotic"),
    ("football.jpg", " Football"),
    ("hearts.jpg", "Hearts"),
    ("pub.jpg", " Pub"),
    ("solidarity.jpg", "Solidarity"),
)

MESSAGE_MODE_CHOICES = (
    ('V', _('View Mode')),
    ('C', _('Chat Mode')),
)

MESSAGE_DISPLAY_TIME_CHOICES = (
    (10, _('10')),
    (20, _('20')),
    (30, _('30')),
    (40, _('40')),
    (50, _('50')),
    (60, _('60')),
)

MODERATOR_MODE_CHOICES = (
    ('A', _('Automatic Mode')),
    ('M', _('Manual Mode')),
)

WAY_SKIRT_CHOICES = (
    (0, _('Never')),
    (3, _('Every 3 receptions')),
    (5, _('Every 5 receptions')),
    (10, _('Every 10 receptions')),
)


class MessageManager(models.Manager):
    def pending(self):
        return self.filter(status='P').select_related()

    def approved(self):
        return self.filter(status='A').select_related()

    def blocked(self):
        return self.filter(status='B').select_related()

    def verified(self):
        query = models.Q(status='B')
        query = query | models.Q(status='B')
        return self.filter(query).select_related()

    def approved_or_published(self):
        return self.filter(models.Q(status='A') | models.Q(status='S')).select_related()


    def published(self):
        return self.filter(status='S').select_related()

    def next_message(self, prefix, prev_id=''):
        if prev_id != 'undefined' and prev_id != '':
            query = models.Q(status='A') | models.Q(status='S', id__gt=prev_id)
            query = self.filter(query)
        else:
            query = self.filter(status='A')

        msgs = query.filter(prefix=prefix).select_related()

        if len(msgs) == 0:
            if prefix.send_random_message:
                random_messages = RandomMessage.objects.all()
                i = randint(0, len(random_messages) - 1)
                try:
                    j = int(prev_id)
                except Exception:
                    j = 0
                random_message = random_messages[i]
                return random_message, j, 'true'
            else:
                last_id = self.published().order_by('-id')[0].id
                return "", last_id, 'false'

        msg = msgs[0]
        msg.status = 'S'
        msg.save()

        return msg.message, msg.id, 'true'


class CensoringWord(models.Model):
    word = models.CharField(primary_key=True, max_length=100)


class CensoringPhone(models.Model):
    number = models.CharField(primary_key=True, max_length=15)


class Prefix(models.Model):
    text = models.CharField(max_length=25, unique=True)

    message_mode = models.CharField(max_length=1, choices=MESSAGE_MODE_CHOICES)
    view_mode_display_time = models.PositiveIntegerField(default=10, choices=MESSAGE_DISPLAY_TIME_CHOICES)
    chat_mode_display_time = models.PositiveIntegerField(default=10, choices=MESSAGE_DISPLAY_TIME_CHOICES)
    moderator_mode = models.CharField(max_length=1, choices=MODERATOR_MODE_CHOICES)
    send_random_message = models.BooleanField(default=False)
    on_air = models.BooleanField(default=False)

    # Colors
    background_image = models.CharField(max_length=25, choices=IMAGE_CHOICES, default='transparent')
    background_color = models.CharField(max_length=7)
    message_color = models.CharField(max_length=7)
    message_text_color = models.CharField(max_length=7)
    message_stripper_color = models.CharField(max_length=7)
    box_background_color = models.CharField(max_length=7)

    # Marquee Color
    marquee_text_color = models.CharField(max_length=7)
    marquee_highlighted_text_color = models.CharField(max_length=7)
    marquee_background_color = models.CharField(max_length=7)

    # Price text color
    price_text_color = models.CharField(max_length=7)

    # Download Instructions
    color_download_instructions = models.CharField(max_length=7)
    color_discharge_words = models.CharField(max_length=7)
    color_number_of_downloads = models.CharField(max_length=7)
    background_color_box_download = models.CharField(max_length=7)

    # Margins
    top_margin = models.PositiveIntegerField()
    left_margin = models.PositiveIntegerField()
    right_margin = models.PositiveIntegerField()
    bottom_margin = models.PositiveIntegerField()

    # Way Skirt
    way_skirt = models.PositiveIntegerField(choices=WAY_SKIRT_CHOICES)

    # Fly
    show_fly = models.BooleanField(default=False)

    # Responses
    ok_response = models.CharField(max_length=160)
    number_censored_response = models.CharField(max_length=160)
    text_censored_response = models.CharField(max_length=160)

    def is_automatic(self):
        return self.moderator_mode == 'A'


    def __unicode__(self):
        return self.text


class TextMessage(models.Model):
    prefix = models.ForeignKey(Prefix)
    message = models.CharField(max_length=160)
    number = models.CharField(max_length=12)
    status = models.CharField(max_length=1, default='P')
    date_time = models.DateTimeField(auto_now_add=True)
    objects = MessageManager()

    def __unicode__(self):
        return self.message


class RandomMessage(models.Model):
    text = models.CharField(max_length=160, unique=True)

    def __unicode__(self):
        return self.text
