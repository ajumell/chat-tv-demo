from django.conf.urls import patterns, url
import views

urlpatterns = patterns(
    '',

    url(r'^$', views.home, name='home'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.sign_out, name='logout'),

    url(r'^add/$', views.add_message_cp, name='add-message-cp'),
    url(r'^messages/add/$', views.add_message, name='add-message'),
    url(r'^messages/manage/$', views.manage_message, name='manage-message'),
    url(r'^messages/block/(?P<message_id>\d+)$', views.block_message, name='block-message'),
    url(r'^messages/approve/(?P<message_id>\d+)$', views.approve_message, name='approve-message'),

    url(r'^censoring/words/$', views.censoring_words, name='censoring-words'),
    url(r'^censoring/words/delete/(?P<word>\w+)/$', views.delete_censoring_word, name='delete-censoring-word'),

    url(r'^censoring/phones/$', views.censoring_phones, name='censoring-phones'),
    url(r'^censoring/phone/delete/(?P<number>\d+)/$', views.delete_censoring_phone, name='delete-censoring-phone'),

    url(r'^messages/random/$', views.random_messages, name='random-messages'),
    url(r'^messages/random/delete/(?P<message_id>\d+)/$', views.delete_random_message, name='delete-random-message'),


    url(r'^preferences/edit/(?P<keyword>\w+)/$', views.edit_preferences, name='edit-preferences'),

    url(r'^chat/show/(?P<keyword>\w+)/$', views.show_chat_window, name='chat'),
    url(r'^chat/preferences/(?P<keyword>\w+)/$', views.get_preference, name='get-preferences'),
    url(r'^chat/get_messages/(?P<keyword>\w+)/$', views.get_message, name='get-messages'),


    # url(r'^$', 'views.home', name='home'),
    # url(r'^/', include('app.urls')),
)
