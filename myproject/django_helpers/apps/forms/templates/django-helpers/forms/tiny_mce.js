tinyMCE.init({
    // General options
    mode : "textareas",
    theme : "advanced",
    plugins : "style,layer,table,insertdatetime,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,preview",
    theme_advanced_buttons4_add : "preview,fullscreen",
    plugin_preview_width : "500",
    plugin_preview_height : "600"
});