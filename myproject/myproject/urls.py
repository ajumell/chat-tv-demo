from django.conf.urls import patterns, include, url
urlpatterns = patterns('',

    url(r'^data-tables/', include('django_helpers.apps.data_tables.urls')),
    url(r'^', include('chat_tv.urls')),
)
